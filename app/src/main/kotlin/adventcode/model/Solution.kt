package adventcode.model

import java.math.BigInteger

interface Solution<T> {
    fun computePartOne(rawInput: List<String>): BigInteger
    fun computePartTwo(rawInput: List<String>): BigInteger
    fun parseInput(rawInput: List<String>): T
}
