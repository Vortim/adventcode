package adventcode.model

interface AdventCalendar {
    fun printResults(): String {
        val yearLabel = "${getYear()}:"
        val padding = getPadding(yearLabel)
        var results = yearLabel + "\n"
        getDays()
            .flatMap { it.printResults() }
            .forEach { results += padding + it + "\n" }
        return results
    }

    fun getPadding(yearLabel: String): String {
        return "".padStart(yearLabel.length, ' ')
    }

    fun getYear(): Int
    fun getDays(): List<AdventDay>
}