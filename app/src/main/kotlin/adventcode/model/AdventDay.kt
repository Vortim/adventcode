package adventcode.model

import adventcode.Utils

private const val NOT_IMPLEMENTED = "NOT IMPLEMENTED"

class AdventDay(
    private val year: Int,
    private val day: Int,
    private val solution: Solution<*>
) {

    private val utils = Utils()

    fun printResults(): List<String> {
        val rawInput: List<String> = utils.readRawInput(year, day)
        return listOf(getPartOneSolution(rawInput), getPartTwoSolution(rawInput))
    }

    private fun getPartTwoSolution(rawInput: List<String>): String {
        return try {
            "$day b: ${solution.computePartTwo(rawInput)}"
        } catch (e: NotImplementedError) {
            NOT_IMPLEMENTED
        }

    }

    private fun getPartOneSolution(rawInput: List<String>): String {
        return try {
            "$day a: ${solution.computePartOne(rawInput)}"
        } catch (e: NotImplementedError) {
            NOT_IMPLEMENTED
        }
    }
}
