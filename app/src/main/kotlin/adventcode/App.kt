package adventcode

import adventcode.c2021.AdventCalendar2021
import adventcode.model.AdventCalendar

private val calendars = listOf<AdventCalendar>(
    AdventCalendar2021()
)

fun main() {
    calendars.forEach { println(it.printResults()) }
}

