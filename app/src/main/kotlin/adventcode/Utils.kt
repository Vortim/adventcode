package adventcode

class Utils {
    fun readRawInput(year: Int, day: Int): List<String> {
        val resourceAsStream = javaClass.classLoader.getResourceAsStream("$year/task${day}Input.txt")
        return String(resourceAsStream.readAllBytes())
            .split(System.lineSeparator())
            .toList()
    }
}
