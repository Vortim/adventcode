package adventcode.c2021

import adventcode.c2021.solutions.solution8.Solution8
import adventcode.model.AdventCalendar
import adventcode.model.AdventDay
import adventcode.solutions.solution1.Solution1
import adventcode.solutions.solution2.Solution2
import adventcode.solutions.solution3.Solution3
import adventcode.solutions.solution4.Solution4
import adventcode.solutions.solution5.Solution5
import adventcode.solutions.solution6.Solution6
import adventcode.solutions.solution7.Solution7

class AdventCalendar2021 : AdventCalendar {

    override fun getYear(): Int {
        return 2021
    }

    override fun getDays(): List<AdventDay> {
        val year = getYear()
        return listOf(
            AdventDay(year, 1, Solution1()),
            AdventDay(year, 2, Solution2()),
            AdventDay(year, 3, Solution3()),
            AdventDay(year, 4, Solution4()),
            AdventDay(year, 5, Solution5()),
            AdventDay(year, 6, Solution6()),
            AdventDay(year, 7, Solution7()),
            AdventDay(year, 8, Solution8()),
        )
    }
}
