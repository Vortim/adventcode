package adventcode.solutions.solution1

import adventcode.model.Solution
import java.math.BigInteger

class Solution1 : Solution<List<Int>> {
    override fun parseInput(rawInput: List<String>): List<Int> {
        return rawInput.filter { it.isNotBlank() }.map { it.toInt() }
    }

    override fun computePartOne(rawInput: List<String>): BigInteger {
        return algorithm(parseInput(rawInput)).toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        val input = parseInput(rawInput)
        val result = ArrayList<Int>()
        for (i in 2 until input.size) {
            result.add(input[i - 2] + input[i - 1] + input[i])
        }
        return algorithm(result).toBigInteger()
    }

    private fun algorithm(input: List<Int>): Int {
        var increments = 0
        var previousValue: Int? = null
        input.forEach {
            if (previousValue != null && previousValue!! < it) {
                increments++
            }
            previousValue = it
        }
        return increments;
    }
}
