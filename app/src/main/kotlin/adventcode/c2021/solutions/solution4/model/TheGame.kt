package adventcode.solutions.solution4.model

import java.lang.System.lineSeparator

data class TheGame(
    val randomNumbers: List<Int>,
    var boards: List<BingoBoard>,
) {
    fun getWinningBoardScore(): Int {
        for (randomNumberIndex in randomNumbers.indices) {
            val randomNumber = randomNumbers[randomNumberIndex]
            mark(randomNumber)
            val winningBoardIndex = getWinningBoardIndex()
            if (winningBoardIndex != null) {
                return compute(boards[winningBoardIndex], randomNumber)
            }
        }
        return -1
    }

    fun getLoosingBoardScore(): Int {
        for (randomNumber in randomNumbers) {
            mark(randomNumber)
            if (boards.size > 1) {
                filterOutWinningBoards()
            } else if (boards[0].isWinning()) {
                return compute(boards[0], randomNumber)
            }
        }
        return -1
    }

    private fun getWinningBoardIndex(): Int? {
        for (boardIndex in boards.indices) {
            if (boards[boardIndex].isWinning()) {
                return boardIndex
            }
        }
        return null
    }

    fun printState(padding: Int): String {
        var result = ""
        for (boardIndex in boards.indices) {
            val board = boards[boardIndex]
            result += "Board number $boardIndex:${lineSeparator()}"
            result += board.printState(padding)
        }
        return result
    }

    private fun filterOutWinningBoards() {
        boards = boards.filter {
            !it.isWinning()
        }
    }

    private fun mark(value: Int) {
        for (board in boards) {
            board.mark(value)
        }
    }

    private fun compute(board: BingoBoard, selectedNumber: Int): Int {
        return board.sumOfUnmarkedNumbers() * selectedNumber
    }
}