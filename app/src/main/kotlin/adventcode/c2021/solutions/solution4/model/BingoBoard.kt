package adventcode.solutions.solution4.model

typealias Row = List<BoardItem>

data class BingoBoard(
    val rows: List<Row>
) {
    fun size(): Int = rows.size
    fun mark(row: Int, col: Int) {
        rows[row][col].mark = true
    }

    fun mark(value: Int) {
        for (row in rows) {
            for (col in row) {
                if (col.value == value) {
                    col.mark = true
                }
            }
        }
    }

    fun printState(padding: Int): String {
        var result = ""
        for (row in rows) {
            for (col in row) {
                val value = col.value.toString()
                val marked = if (col.mark) "t" else ""
                val rawValue = value + marked
                result += rawValue.padStart(padding, ' ')
            }
            result += System.lineSeparator()
        }
        return result
    }

    fun isWinning(): Boolean {
        return checkRowsForWinning() || checkColsForWinning()
    }

    private fun checkColsForWinning(): Boolean {
        for (columnIndex in rows[0].indices) {
            if (isWinningColumn(columnIndex)) {
                return true
            }
        }
        return false
    }

    private fun isWinningColumn(columnIndex: Int): Boolean {
        for (row in rows) {
            if (!row[columnIndex].mark) {
                return false
            }
        }
        return true
    }

    private fun checkRowsForWinning(): Boolean {
        for (row in rows) {
            val winningRow = isWinningRow(row)
            if (winningRow) {
                return true
            }
        }
        return false
    }

    private fun isWinningRow(row: List<BoardItem>): Boolean {
        for (item in row) {
            if (!item.mark) {
                return false
            }
        }
        return true
    }

    fun sumOfUnmarkedNumbers(): Int {
        var result = 0
        for (row in rows) {
            for (col in row) {
                if (!col.mark) {
                    result += col.value
                }
            }
        }
        return result
    }
}