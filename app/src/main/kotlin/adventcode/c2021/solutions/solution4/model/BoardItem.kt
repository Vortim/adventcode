package adventcode.solutions.solution4.model

data class BoardItem(
    val value: Int,
    var mark: Boolean = false
)