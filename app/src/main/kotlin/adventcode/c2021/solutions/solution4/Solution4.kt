package adventcode.solutions.solution4

import adventcode.model.Solution
import adventcode.solutions.solution4.model.BingoBoard
import adventcode.solutions.solution4.model.BoardItem
import adventcode.solutions.solution4.model.Row
import adventcode.solutions.solution4.model.TheGame
import java.math.BigInteger

class Solution4 : Solution<TheGame> {
    override fun computePartOne(rawInput: List<String>): BigInteger {
        return parseInput(rawInput).getWinningBoardScore().toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        return parseInput(rawInput).getLoosingBoardScore().toBigInteger()
    }

    override fun parseInput(rawInput: List<String>): TheGame {
        val randomNumbers = parseRandomNumbers(rawInput)
        val boards = parseBoards(rawInput)

        return TheGame(
                randomNumbers = randomNumbers,
                boards = boards
        )
    }

    private fun parseBoards(rawInput: List<String>): List<BingoBoard> {
        var currentIndex = 2
        val result = mutableListOf<BingoBoard>()
        while (currentIndex <= rawInput.size) {
            val board = parseBoard(currentIndex, rawInput)
            result.add(board)
            currentIndex += board.size() + 1
        }
        return result
    }

    private fun parseBoard(boardStartIndex: Int, rawInput: List<String>): BingoBoard {
        var index = boardStartIndex
        val rows = mutableListOf<Row>()
        while (index < rawInput.size && rawInput[index].isNotBlank()) {
            val row = rawInput[index]
                .split(" ")
                .map { it.trim() }
                .filter { it.isNotBlank() }
                .map { BoardItem(it.toInt()) }
                .toList()
            rows.add(row)
            index++
        }
        return BingoBoard(rows)
    }

    private fun parseRandomNumbers(rawInput: List<String>) =
        rawInput[0].split(",").map { it.toInt() }.toList()
}
