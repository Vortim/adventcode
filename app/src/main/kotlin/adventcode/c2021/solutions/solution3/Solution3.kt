package adventcode.solutions.solution3

import adventcode.model.Solution
import java.math.BigInteger

class Solution3 : Solution<List<List<Int>>> {

    override fun computePartOne(rawInput: List<String>): BigInteger {
        return algorithm(parseInput(rawInput)).toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        val input = parseInput(rawInput)

        val bitFilters = calculateBitFilters(input)
        val oxygenRating = filterByBits(bitFilters, input, 0, false)
        val co2Rating = filterByBits(bitFilters, input, 0, true)

        return (binaryToDecimal(oxygenRating) * binaryToDecimal(co2Rating)).toBigInteger()
    }

    private fun calculateBitFilters(input: List<List<Int>>): List<Int> {
        return toBits(calculateBitPresence(input), input.size)
    }

    private fun filterByBits(
        bitFilters: List<Int>,
        input: List<List<Int>>,
        index: Int,
        leastCommon: Boolean
    ): List<Int> {
        var filterBits = bitFilters
        if (leastCommon) {
            filterBits = inverse(bitFilters)
        }

        if (input.size == 1) {
            return input[0]
        }
        val newInput = input.filter {
            it[index] == filterBits[index]
        }
        return filterByBits(calculateBitFilters(newInput), newInput, index + 1, leastCommon)
    }

    override fun parseInput(rawInput: List<String>): List<List<Int>> {
        return rawInput
            .filter { it.isNotBlank() }
            .map {
                it.toList()
                    .map { it2 -> it2.toString().toInt() }
            }
    }

    private fun algorithm(input: List<List<Int>>): Int {
        val memory = calculateBitPresence(input)
        val gammaRate = toBits(memory, input.size)
        val epsilonRate = inverse(gammaRate)
        return binaryToDecimal(gammaRate) * binaryToDecimal(epsilonRate)
    }

    private fun calculateBitPresence(input: List<List<Int>>): MutableMap<Int, Int> {
        val memory = mutableMapOf<Int, Int>()
        input.forEach {
            for (bitIndex in it.indices) {
                memory.merge(bitIndex, it[bitIndex]) { prev, one ->
                    prev + one
                }
            }
        }
        return memory
    }

    private fun toBits(memory: MutableMap<Int, Int>, total: Int): List<Int> {
        return memory.entries
            .map { entry -> if (entry.value >= (total / 2.toDouble())) 1 else 0 }
            .toList()
    }

    private fun inverse(gammaRate: List<Int>): List<Int> {
        return gammaRate
            .map { if (it == 1) 0 else 1 }
            .toList()
    }

    private fun binaryToDecimal(bits: List<Int>): Int {
        val binary = bits.joinToString("").toLong()
        return convertBinaryToDecimal(binary)
    }

    private fun convertBinaryToDecimal(num: Long): Int {
        var num = num
        var decimalNumber = 0
        var i = 0
        var remainder: Long

        while (num.toInt() != 0) {
            remainder = num % 10
            num /= 10
            decimalNumber += (remainder * Math.pow(2.0, i.toDouble())).toInt()
            ++i
        }
        return decimalNumber
    }
}
