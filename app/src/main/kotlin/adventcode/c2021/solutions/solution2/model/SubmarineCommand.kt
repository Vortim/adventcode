package adventcode.solutions.solution2.model

data class SubmarineCommand(
        val direction: Direction,
        val amount: Int
)
