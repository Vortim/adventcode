package adventcode.solutions.solution2.model

enum class Direction(val modifier: Int, val horizontal: Boolean) {
    FORWARD(1, true),
    DOWN(1, false),
    UP(-1, false);
}
