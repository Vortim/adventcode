package adventcode.solutions.solution2

import adventcode.model.Solution
import adventcode.solutions.solution2.model.Direction
import adventcode.solutions.solution2.model.SubmarineCommand
import java.math.BigInteger

class Solution2 : Solution<List<SubmarineCommand>> {

    override fun parseInput(rawInput: List<String>): List<SubmarineCommand> {
        return rawInput
                .filter { it.isNotBlank() }
                .map {
                    val split = it.split(" ")
                    SubmarineCommand(direction = Direction.valueOf(split[0].uppercase()), amount = split[1].toInt())
                }
    }

    override fun computePartOne(rawInput: List<String>): BigInteger {
        return algorithm(parseInput(rawInput), false).toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        return algorithm(parseInput(rawInput), true).toBigInteger()
    }

    private fun algorithm(readInput: List<SubmarineCommand>, withAim: Boolean): Int {
        var horizontal = 0
        var depth = 0
        var aim = 0

        for (submarineCommand in readInput) {
            val direction = submarineCommand.direction
            val change = submarineCommand.amount * direction.modifier
            if (direction.horizontal) {
                if (withAim) {
                    depth += change * aim
                }
                horizontal += change
            } else {
                if (withAim) {
                    aim += change
                } else {
                    depth += change
                }
            }
        }

        return horizontal * depth
    }
}

