package adventcode.c2021.solutions.solution8

import adventcode.model.Solution
import java.math.BigInteger

class Solution8 : Solution<List<String>> {
    override fun computePartOne(rawInput: List<String>): BigInteger {
        return rawInput
            .map { it.split(" | ") }
            .filter { it.size < 2 }
            .map { it[1] }
            .flatMap { it.split(" ") }
            .map { it.trim() }
            .filter { it.length == 2 || it.length == 4 || it.length == 3 || it.length == 7 }
            .count()
            .toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        TODO("Not yet implemented")
    }

    override fun parseInput(rawInput: List<String>): List<String> {
        return rawInput
    }
}
