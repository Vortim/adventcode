import adventcode.solutions.solution5.model.Line
import java.lang.System.lineSeparator

typealias Row = MutableMap<Int, Int>

data class Grid(var rows: List<Row> = mutableListOf()) {
    fun printGrid(padding: Int): String {
        var result = ""
        for (row in rows) {
            for (crossingLines in row) {
                val valueToPrint = if (crossingLines.value == 0) "." else crossingLines.value.toString()
                result += valueToPrint.padStart(padding, ' ')
            }
            result += lineSeparator()
        }
        return result
    }

    fun results(thresholdInclusively: Int): Int {
        var result = 0;
        for (row in rows) {
            for (col in row) {
                if (col.value >= thresholdInclusively) {
                    result++
                }
            }
        }
        return result
    }

    fun setLines(lines: List<Line>, diagonalSupported: Boolean) {
        rows = initialize(lines)
        lines
            .filter {
                it.isHorizontal() || it.isVertical() || (diagonalSupported && it.isDiagonal())
            }
            .flatMap {
                it.pointMakingLine()
            }.forEach {
                val currentValue: Int = rows[it.y][it.x] ?: 0
                rows[it.y][it.x] = currentValue + 1
            }
    }

    private fun initialize(lines: List<Line>): List<Row> {
        val maxX = computeLargestX(lines)
        val maxY = computeLargestY(lines)
        val rows = mutableListOf<Row>()
        for (y in 0..maxY) {
            val row = mutableMapOf<Int, Int>()
            for (x in 0..maxX) {
                row[x] = 0
            }
            rows.add(row)
        }
        return rows
    }

    private fun computeLargestX(lines: List<Line>): Int {
        return lines.flatMap {
            listOf(it.start, it.end)
        }.map {
            it.x
        }.maxOf { it }
    }

    private fun computeLargestY(lines: List<Line>): Int {
        return lines.flatMap {
            listOf(it.start, it.end)
        }.map {
            it.y
        }.maxOf { it }
    }
}