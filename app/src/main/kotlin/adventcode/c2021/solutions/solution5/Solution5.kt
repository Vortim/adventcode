package adventcode.solutions.solution5

import Grid
import adventcode.model.Solution
import adventcode.solutions.solution5.model.Line
import adventcode.solutions.solution5.model.Point
import java.math.BigInteger

private const val LINE_SEPARATOR = " -> "
private const val POINT_SEPARATOR = ","

class Solution5 : Solution<List<Line>> {
    override fun computePartOne(rawInput: List<String>): BigInteger {
        val grid = Grid()
        grid.setLines(parseInput(rawInput), false)
        return grid.results(thresholdInclusively = 2).toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        val grid = Grid()
        grid.setLines(parseInput(rawInput), true)
        return grid.results(thresholdInclusively = 2).toBigInteger()
    }

    override fun parseInput(rawInput: List<String>): List<Line> {
        return rawInput
            .filter { it.isNotBlank() }
            .map {
                val points = it.split(LINE_SEPARATOR)
                Line(parsePoint(points[0]), parsePoint(points[1]))
            }.toList()
    }

    private fun parsePoint(input: String): Point {
        val values = input.split(POINT_SEPARATOR)
        return Point(
            x = values[0].toInt(),
            y = values[1].toInt()
        )
    }
}

