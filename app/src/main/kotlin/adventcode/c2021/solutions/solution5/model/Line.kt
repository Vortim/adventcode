package adventcode.solutions.solution5.model

import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

data class Line(
    val start: Point,
    val end: Point
) {
    fun isHorizontal(): Boolean {
        return start.y == end.y
    }

    fun isVertical(): Boolean {
        return start.x == end.x
    }

    fun isDiagonal(): Boolean {
        return abs(start.x - end.x) == abs(start.y - end.y)
    }

    fun pointMakingLine(): List<Point> {
        val result = mutableListOf<Point>()
        if (isHorizontal()) {
            for (x in min(start.x, end.x)..max(start.x, end.x)) {
                result.add(Point(x, start.y))
            }
        }
        if (isVertical()) {
            for (y in min(start.y, end.y)..max(start.y, end.y)) {
                result.add(Point(start.x, y))
            }
        }
        if (isDiagonal()) {
            for (i in 0..abs(start.x - end.x)) {
                val left = getLeftPoint()
                val right = getRightPoint()
                result.add(Point(left.x + i, if (left.y < right.y) left.y + i else left.y - i))
            }
        }
        return result
    }

    private fun getLeftPoint(): Point = if (start.x < end.x) start else end
    private fun getRightPoint(): Point = if (start.x < end.x) end else start

}