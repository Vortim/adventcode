package adventcode.solutions.solution7

import adventcode.model.Solution
import java.math.BigInteger
import kotlin.math.abs

class Solution7 : Solution<List<Int>> {

    override fun computePartOne(rawInput: List<String>): BigInteger {
        val input = parseInput(rawInput)
        val alignPosition = input[input.size / 2]

        var result = 0
        input.forEach { result += abs(it - alignPosition) }
        return result.toBigInteger()
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        val input = parseInput(rawInput)

        val results = mutableMapOf<Int, Int>()
        for (i in input.indices) {
            results[i] = computeFuelConsumption(input, i)
        }

        return results.values.minOf { it }.toBigInteger()
    }

    private fun computeFuelConsumption(input: List<Int>, position: Int): Int {
        var result = 0
        input.forEach {
            val n = abs(it - position)
            result += n * (n + 1) / 2
        }
        return result
    }

    override fun parseInput(rawInput: List<String>): List<Int> {
        return rawInput
            .flatMap { it.split(",") }
            .filter { it.isNotBlank() }
            .map { it.toInt() }
            .sorted()
    }
}