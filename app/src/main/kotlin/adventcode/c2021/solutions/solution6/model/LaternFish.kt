package adventcode.solutions.solution6.model

data class LaternFish(var daysToSpawn: Int = 8) {
    fun nextDay(): List<LaternFish> {
        val nextDayValue = daysToSpawn - 1
        if (nextDayValue < 0) {
            return listOf(LaternFish(6), LaternFish())
        }
        return listOf(LaternFish(nextDayValue))

    }
}
