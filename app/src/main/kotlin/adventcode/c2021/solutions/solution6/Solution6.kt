package adventcode.solutions.solution6

import adventcode.model.Solution
import adventcode.solutions.solution6.model.LaternFish
import java.math.BigInteger
import java.math.BigInteger.ZERO

class Solution6 : Solution<List<LaternFish>> {

    override fun computePartOne(rawInput: List<String>): BigInteger {
        return computeV2(rawInput, 80)
    }

    override fun computePartTwo(rawInput: List<String>): BigInteger {
        return computeV2(rawInput, 256)
    }

    private fun computeV2(rawInput: List<String>, days: Int): BigInteger {
        var populations = parseInput(rawInput)
                .groupBy(LaternFish::daysToSpawn) { 1 }
                .mapValues { entry -> entry.value.size.toBigInteger() }

        for (i in 0 until days) {
            val newPopulation = mapOf(
                    0 to populations.getOrDefault(1, ZERO),
                    1 to populations.getOrDefault(2, ZERO),
                    2 to populations.getOrDefault(3, ZERO),
                    3 to populations.getOrDefault(4, ZERO),
                    4 to populations.getOrDefault(5, ZERO),
                    5 to populations.getOrDefault(6, ZERO),
                    6 to (populations.getOrDefault(7, ZERO) + populations.getOrDefault(0, ZERO)),
                    7 to populations.getOrDefault(8, ZERO),
                    8 to populations.getOrDefault(0, ZERO)
            )
            populations = newPopulation
        }

        var result = ZERO
        populations.values.forEach { result += it }
        return result
    }

    private fun compute(rawInput: List<String>, days: Int): BigInteger {
        var state = parseInput(rawInput)
        for (i in 0 until days) {
            state = state.flatMap { it.nextDay() }
        }
        return state.size.toBigInteger()
    }

    override fun parseInput(rawInput: List<String>): List<LaternFish> {
        return rawInput
                .filter { it.isNotBlank() }
                .flatMap { it.split(",") }
                .map { LaternFish(it.toInt()) }
                .toList()
    }
}
