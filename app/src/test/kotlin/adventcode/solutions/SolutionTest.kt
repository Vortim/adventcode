package adventcode.solutions

import adventcode.model.Solution
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigInteger

abstract class SolutionTest<T> {

    @Test
    fun `should verify part one`() {
        val solution = getSolution()

        val result = solution.computePartOne(getInput())

        result shouldBe getExpectedPartOneValue()
    }

    @Test
    fun `should verify part two`() {
        val solution = getSolution()

        val result = solution.computePartTwo(getInput())

        result shouldBe getExpectedPartTwoValue()
    }

    @Test
    fun `should verify parsing input`() {
        val solution = getSolution()

        val parsedInput = solution.parseInput(getInput())

        parsedInput shouldBe getExpectedParsedInput()
    }

    abstract fun getSolution(): Solution<T>

    abstract fun getInput(): List<String>

    abstract fun getExpectedParsedInput(): T

    abstract fun getExpectedPartOneValue(): BigInteger

    abstract fun getExpectedPartTwoValue(): BigInteger
}
