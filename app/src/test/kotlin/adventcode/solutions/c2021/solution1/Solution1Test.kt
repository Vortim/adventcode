package adventcode.solutions.c2021.solution1

import adventcode.solutions.solution1.Solution1
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigInteger

class Solution1Test {

    private val task: Solution1 = Solution1()

    private val input: List<String> = listOf(
            "199",
            "200",
            "208",
            "210",
            "200",
            "207",
            "240",
            "269",
            "260",
            "263",
    )

    @Test
    fun `should verify first part of a task`() {
        val result = task.computePartOne(input)

        result shouldBe BigInteger.valueOf(7)
    }

    @Test
    fun `should verify second part of a task`() {
        val result = task.computePartTwo(input)

        result shouldBe BigInteger.valueOf(5)
    }
}
