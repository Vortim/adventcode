package adventcode.solutions.c2021.solution3

import adventcode.solutions.solution3.Solution3
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigInteger

class Solution3Test {

    private val task = Solution3()

    private val input: List<String> = listOf(
            "00100",
            "11110",
            "10110",
            "10111",
            "10101",
            "01111",
            "00111",
            "11100",
            "10000",
            "11001",
            "00010",
            "01010",
    )

    @Test
    fun `should verify task 3 part one`() {
        val result = task.computePartOne(input)

        result shouldBe BigInteger.valueOf(198)
    }

    @Test
    fun `should verify task 3 part two`() {
        val result = task.computePartTwo(input)

        result shouldBe BigInteger.valueOf(230)
    }
}
