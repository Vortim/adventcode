package adventcode.solutions.c2021.solution4

import adventcode.solutions.c2021.solution4.GameFactory.Companion.getExampleGame
import adventcode.solutions.solution4.model.BingoBoard
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class BingoBoardTest {
    @Test
    fun `should be marked as winning when row is marked`() {
        val board = getExampleGame().boards[0]
        for (i in board.rows[2].indices) {
            board.mark(2, i)
        }

        val winning = board.isWinning()

        winning shouldBe true
    }

    @Test
    fun `should be marked as winning when column is marked`() {
        val board = getExampleGame().boards[0]
        for (i in board.rows.indices) {
            board.mark(i, 2)
        }

        val winning = board.isWinning()

        winning shouldBe true
    }

    @Test
    fun `should be marked as not winning when there is no full row or col`() {
        val winning = boardWithNoWinning().isWinning()

        winning shouldBe false
    }

    @Test
    fun `should print current board state`() {
        val boardState = boardWithNoWinning().printState(5)

        boardState shouldBe """
            |   22  13t  17t  11t   0t
            |   8t    2  23t   4t  24t
            |  21t   9t   14  16t   7t
            |   6t  10t   3t   18   5t
            |   1t  12t  20t  15t   19
            |
        """.trimMargin()
    }

    private fun boardWithNoWinning(): BingoBoard {
        val board = getExampleGame().boards[0]
        for (rowIndex in board.rows.indices) {
            val row = board.rows[rowIndex]
            for (colIndex in row.indices) {
                if (rowIndex != colIndex) {
                    board.mark(rowIndex, colIndex)
                }
            }
        }

        return board
    }
}