package adventcode.solutions.c2021.solution4

import adventcode.solutions.solution4.model.BingoBoard
import adventcode.solutions.solution4.model.BoardItem
import adventcode.solutions.solution4.model.TheGame

class GameFactory {
    companion object {
        @JvmStatic
        fun getExampleGame(): TheGame {
            return TheGame(
                randomNumbers = listOf(
                    7,
                    4,
                    9,
                    5,
                    11,
                    17,
                    23,
                    2,
                    0,
                    14,
                    21,
                    24,
                    10,
                    16,
                    13,
                    6,
                    15,
                    25,
                    12,
                    22,
                    18,
                    20,
                    8,
                    19,
                    3,
                    26,
                    1
                ),
                boards = listOf(
                    BingoBoard(
                        rows = listOf(
                            listOf(BoardItem(22), BoardItem(13), BoardItem(17), BoardItem(11), BoardItem(0)),
                            listOf(BoardItem(8), BoardItem(2), BoardItem(23), BoardItem(4), BoardItem(24)),
                            listOf(BoardItem(21), BoardItem(9), BoardItem(14), BoardItem(16), BoardItem(7)),
                            listOf(BoardItem(6), BoardItem(10), BoardItem(3), BoardItem(18), BoardItem(5)),
                            listOf(BoardItem(1), BoardItem(12), BoardItem(20), BoardItem(15), BoardItem(19)),
                        )
                    ),
                    BingoBoard(
                        rows = listOf(
                            listOf(BoardItem(3), BoardItem(15), BoardItem(0), BoardItem(2), BoardItem(22)),
                            listOf(BoardItem(9), BoardItem(18), BoardItem(13), BoardItem(17), BoardItem(5)),
                            listOf(BoardItem(19), BoardItem(8), BoardItem(7), BoardItem(25), BoardItem(23)),
                            listOf(BoardItem(20), BoardItem(11), BoardItem(10), BoardItem(24), BoardItem(4)),
                            listOf(BoardItem(14), BoardItem(21), BoardItem(16), BoardItem(12), BoardItem(6)),
                        )
                    ),
                    BingoBoard(
                        rows = listOf(
                            listOf(BoardItem(14), BoardItem(21), BoardItem(17), BoardItem(24), BoardItem(4)),
                            listOf(BoardItem(10), BoardItem(16), BoardItem(15), BoardItem(9), BoardItem(19)),
                            listOf(BoardItem(18), BoardItem(8), BoardItem(23), BoardItem(26), BoardItem(20)),
                            listOf(BoardItem(22), BoardItem(11), BoardItem(13), BoardItem(6), BoardItem(5)),
                            listOf(BoardItem(2), BoardItem(0), BoardItem(12), BoardItem(3), BoardItem(7)),
                        )
                    )
                )
            )
        }
    }
}