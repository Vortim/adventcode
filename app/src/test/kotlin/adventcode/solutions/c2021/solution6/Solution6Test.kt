package adventcode.solutions.c2021.solution6

import adventcode.model.Solution
import adventcode.solutions.SolutionTest
import adventcode.solutions.solution6.Solution6
import adventcode.solutions.solution6.model.LaternFish
import java.math.BigInteger

class Solution6Test : SolutionTest<List<LaternFish>>() {
    private val solution = Solution6()

    override fun getInput(): List<String> =
        listOf(
            "3,4,3,1,2",
        )

    override fun getSolution(): Solution<List<LaternFish>> = solution

    override fun getExpectedParsedInput(): List<LaternFish> = listOf(
        LaternFish(3),
        LaternFish(4),
        LaternFish(3),
        LaternFish(1),
        LaternFish(2),
    )

    override fun getExpectedPartOneValue() = BigInteger.valueOf(5934)

    override fun getExpectedPartTwoValue() = BigInteger.valueOf(26984457539)

}
