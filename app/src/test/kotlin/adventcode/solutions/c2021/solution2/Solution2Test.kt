package adventcode.solutions.c2021.solution2

import adventcode.solutions.solution2.Solution2
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigInteger

class Solution2Test {

    private val task = Solution2()

    private val input: List<String> = listOf(
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
    )

    @Test
    fun `should verify task 2 algorithm without aiming`() {
        val result = task.computePartOne(input)

        result shouldBe BigInteger.valueOf(150)
    }

    @Test
    fun `should verify task 2 algorithm with aiming`() {
        val result = task.computePartTwo(input)

        result shouldBe BigInteger.valueOf(900)
    }
}
