package adventcode.solutions.c2021.solution5

import adventcode.solutions.solution5.Solution5
import adventcode.solutions.solution5.model.Line
import adventcode.solutions.solution5.model.Point
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigInteger

class Solution5Test {


    private val task = Solution5()

    private val input: List<String> = listOf(
        "0,9 -> 5,9",
        "8,0 -> 0,8",
        "9,4 -> 3,4",
        "2,2 -> 2,1",
        "7,0 -> 7,4",
        "6,4 -> 2,0",
        "0,9 -> 2,9",
        "3,4 -> 1,4",
        "0,0 -> 8,8",
        "5,5 -> 8,2",
    )

    @Test
    fun `should verify task 5 part one`() {
        val result = task.computePartOne(input)

        result shouldBe BigInteger.valueOf(5)
    }

    @Test
    fun `should verify task 5 part two`() {
        val result = task.computePartTwo(input)

        result shouldBe BigInteger.valueOf(12)
    }

    @Test
    fun `should verify parsing input`() {
        val input = task.parseInput(input)

        input shouldBe listOf(
            Line(Point(0, 9), Point(5, 9)),
            Line(Point(8, 0), Point(0, 8)),
            Line(Point(9, 4), Point(3, 4)),
            Line(Point(2, 2), Point(2, 1)),
            Line(Point(7, 0), Point(7, 4)),
            Line(Point(6, 4), Point(2, 0)),
            Line(Point(0, 9), Point(2, 9)),
            Line(Point(3, 4), Point(1, 4)),
            Line(Point(0, 0), Point(8, 8)),
            Line(Point(5, 5), Point(8, 2)),
        )
    }
}