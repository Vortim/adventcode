package adventcode.solutions.c2021.solution5.model

import adventcode.solutions.solution5.model.Line
import adventcode.solutions.solution5.model.Point
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class LineTest {

    private val horizontalLine = Line(Point(1, 6), Point(3, 6))
    private val verticalLine = Line(Point(1, 3), Point(1, 5))
    private val diagonalLine = Line(Point(9, 7), Point(7, 9))
    private val diagonalLine2 = Line(Point(1, 1), Point(3, 3))

    @Test
    fun `should be horizontal`() {
        val horizontal = horizontalLine.isHorizontal()

        horizontal shouldBe true
    }

    @Test
    fun `should not be horizontal`() {
        val horizontal = verticalLine.isHorizontal()

        horizontal shouldBe false
    }

    @Test
    fun `should return points making horizontal line`() {
        val points = horizontalLine.pointMakingLine()

        points shouldContainExactly listOf(
            Point(1, 6),
            Point(2, 6),
            Point(3, 6),
        )
    }

    @Test
    fun `should return points making horizontal line backwards`() {
        val line = Line(horizontalLine.end, horizontalLine.start)

        val points = line.pointMakingLine()

        points shouldContainExactly listOf(
            Point(1, 6),
            Point(2, 6),
            Point(3, 6),
        )
    }

    @Test
    fun `should be vertical`() {
        val vertical = verticalLine.isVertical()

        vertical shouldBe true
    }

    @Test
    fun `should not be vertical`() {
        val vertical = horizontalLine.isVertical()

        vertical shouldBe false
    }

    @Test
    fun `should return points making vertical line`() {
        val points = verticalLine.pointMakingLine()

        points shouldContainExactly listOf(
            Point(1, 3),
            Point(1, 4),
            Point(1, 5),
        )
    }

    @Test
    fun `should return points making vertical line backwards`() {
        val line = Line(verticalLine.end, verticalLine.start)

        val points = line.pointMakingLine()

        points shouldContainExactly listOf(
            Point(1, 3),
            Point(1, 4),
            Point(1, 5),
        )
    }

    @Test
    fun `should recognise diagonal line`() {
        diagonalLine.isHorizontal() shouldBe false
        diagonalLine.isVertical() shouldBe false
        diagonalLine.isDiagonal() shouldBe true
    }

    @Test
    fun `should recognise diagonal line 2`() {
        diagonalLine2.isHorizontal() shouldBe false
        diagonalLine2.isVertical() shouldBe false
        diagonalLine2.isDiagonal() shouldBe true
    }

    @Test
    fun `should return points making diagonal line`() {
        val points = diagonalLine.pointMakingLine()

        points shouldContainExactly listOf(
            Point(7, 9),
            Point(8, 8),
            Point(9, 7),
        )
    }

    @Test
    fun `should return points making diagonal line backwards`() {
        val line = Line(diagonalLine.end, diagonalLine.start)

        val points = line.pointMakingLine()

        points shouldContainExactly listOf(
            Point(7, 9),
            Point(8, 8),
            Point(9, 7),
        )
    }

    @Test
    fun `should return points making diagonal line 2`() {
        val points = diagonalLine2.pointMakingLine()

        points shouldContainExactly listOf(
            Point(1, 1),
            Point(2, 2),
            Point(3, 3),
        )
    }

    @Test
    fun `should return points making diagonal line backwards 2`() {
        val line = Line(diagonalLine2.end, diagonalLine2.start)

        val points = line.pointMakingLine()

        points shouldContainExactly listOf(
            Point(1, 1),
            Point(2, 2),
            Point(3, 3),
        )
    }
}