package adventcode.solutions.c2021.solution7

import adventcode.solutions.solution7.Solution7
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigInteger

class Solution7Test {
    private val task = Solution7()

    private val input: List<String> = listOf(
        "16,1,2,0,4,2,7,1,2,14",
    )

    @Test
    fun `should verify task 6 part one`() {
        val result = task.computePartOne(input)

        result shouldBe BigInteger.valueOf(37)
    }

    @Test
    fun `should verify task 6 part two`() {
        val result = task.computePartTwo(input)

        result shouldBe BigInteger.valueOf(168)
    }

}