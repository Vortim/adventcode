package adventcode.c2021.solutions.solution8

import adventcode.model.Solution
import adventcode.solutions.SolutionTest
import java.math.BigInteger

class Solution8Test : SolutionTest<List<String>>() {
    override fun getSolution(): Solution<List<String>> = Solution8()

    override fun getInput(): List<String> =
        listOf("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf")

    override fun getExpectedParsedInput(): List<String> {
        TODO("Not yet implemented")
    }

    override fun getExpectedPartOneValue(): BigInteger {
        TODO("Not yet implemented")
    }

    override fun getExpectedPartTwoValue(): BigInteger {
        TODO("Not yet implemented")
    }
}
